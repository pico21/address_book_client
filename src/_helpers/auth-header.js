export function authHeader() {
    // return authorization header with jwt token
    let wsse = JSON.parse(localStorage.getItem('wsse'));

    if (wsse) {
        let wsse_header = 'UsernameToken Username="'+wsse.Username+'", PasswordDigest="'+wsse.PasswordDigest+'", Nonce="'+wsse.Nonce+'", Created="'+wsse.Created+'"';
        return { 'x-wsse': wsse_header };
    } else {
        return { };
    }
}