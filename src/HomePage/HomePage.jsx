import React from 'react';
import { connect } from 'react-redux';
import * as contactActions from "../_actions/";
import MUIDataTable from "mui-datatables";
import { CircularProgress, Typography } from '@material-ui/core';

class HomePage extends React.Component {
    componentDidMount() {

        this.getContacts();
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteContacts(id);
    }

    render() {
        const { contacts } = this.props;

        const options = {
            filter: true,
            filterType: 'dropdown',
            responsive: 'stacked',
            rowsPerPageOptions: [5,10,20,50],
            serverSide: true,
            count: contacts.contacts_arguments ? contacts.contacts_arguments.total : 0,
            page: contacts.contacts_arguments ? contacts.contacts_arguments.page : 0,
            onSearchChange: (searchText) => {

            },
            onTableChange: (action, tableState) => {

                switch (action) {
                    case 'changePage':
                        this.props.changePage(this.props.contacts.contacts_arguments, tableState.page);
                        this.getContacts();
                        break;
                }
            },
            onChangeRowsPerPage: (number) => {

                this.props.changeRowForPage(this.props.contacts.contacts_arguments, number);
                this.getContacts();
            }
        };

        return (

            <div className="col-md-16 col-md-offset-16">

                <MUIDataTable
                    title={"Address Book"}
                    data={contacts.items}
                    columns={columns}
                    options={options}
                />

            </div>
        );
    }

    getContacts(){
        const args = this.props.contacts.contacts_arguments;
        this.props.getContacts(args);
    };

}




function mapStateToProps(state) {

    const { contacts, authentication, contacts_arguments } = state;

    return { contacts, authentication, contacts_arguments};
}

const actionCreators = {
    getContacts:    contactActions.getContacts(),
    deleteContacts: contactActions.deleteContacts(),
    saveContacts:   contactActions.saveContacts(),
    changePage:      contactActions.changePage(),
    changeRowForPage: contactActions.changeRowForPage(),
};


const columns = [
    {
        name: "id",
        label: "id",
        options: {
            display: 'excluded'
        }
    },
    {
        name: "firstname",
        label: "Nome",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "lastname",
        label: "Cognome",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "phones",
        label: "Telefono",
        options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <div>{value && value.map((elem, index) => {return elem.number;}).join(", ")}</div>
                );
            },
        }
    },

    {
        name: "emails",
        label: "Email",
        options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <div>{value && value.map((elem, index) => {return elem.email;}).join(", ")}</div>
                );
            },
        }
    },
    {
        name: "tags",
        label: "Tags",
        options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <div>{value && value.map((elem, index) => {return elem.name;}).join(", ")}</div>
                );
            },
        }
    },
    {
        name: "socials",
        label: "Socials",
        options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
                return (
                    <div>{value && value.map((elem, index) => {return elem.type;}).join(", ")}</div>
                );
            },
        }
    },
    {
        name: "add_date",
        label: "Creato il",
        options: {
            filter: true,
            sort: true,
        }
    },
];

const connectedHomePage = connect(mapStateToProps, contactActions)(HomePage);
export { connectedHomePage as HomePage };