const validate = values => {
    const errors = {};

    if (!values.Contacts || !values.Contacts.length) {
        errors.Contacts = { _error: 'At least one contact must be entered' }
    } else {
        const contactsArrayErrors = [];
        values.Contacts.forEach((contact, contactIndex) => {
            const contactErrors = {};
            if (!contact || !contact.firstname) {
                contactErrors.firstname = 'Required';
                contactsArrayErrors[contactIndex] = contactErrors
            }
            if (!contact || !contact.lastname) {
                contactErrors.lastname = 'Required';
                contactsArrayErrors[contactIndex] = contactErrors
            }

            if (contact && contact.emails && contact.emails.length) {
                const emailArrayErrors = [];
                contact.emails.forEach((email, emailIndex) => {
                    if (!email || !email.length) {
                        emailArrayErrors[emailIndex] = 'Required'
                    }
                })
                if (emailArrayErrors.length) {
                    contactErrors.emails = emailArrayErrors;
                    contactsArrayErrors[contactIndex] = contactErrors
                }
                // if (contact.emails.length > 5) {
                //     if (!contactErrors.emails) {
                //         contactErrors.emails = []
                //     }
                //     contactErrors.emails._error = 'No more than five Emails allowed'
                //     contactsArrayErrors[contactIndex] = contactErrors
                // }
            }

            if (contact && contact.phones && contact.phones.length) {
                const phoneArrayErrors = [];
                contact.phones.forEach((phone, phoneIndex) => {
                    if (!phone || !phone.length) {
                        phoneArrayErrors[phoneIndex] = 'Required'
                    }
                });
                if (phoneArrayErrors.length) {
                    contactErrors.phones = phoneArrayErrors;
                    contactsArrayErrors[contactIndex] = contactErrors
                }
            }

        });
        if (contactsArrayErrors.length) {
            errors.Contacts = contactsArrayErrors
        }
    }
    return errors
};

export default validate;