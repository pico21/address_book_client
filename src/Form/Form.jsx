import React from 'react'
import { Field, FieldArray, reduxForm, Form } from 'redux-form'
import { makeStyles } from '@material-ui/core/styles';
import {Button, List, ListItem, Divider, TextField, Card ,CardContent} from '@material-ui/core'
import validate from "./validate"
import {contacts} from "../_reducers/contacts.reducer";

// const renderField = ({ input, label, type, meta: { touched, error } }) => (
//     <div>
//         <label>{label}</label>
//         <div>
//             <input {...input} type={type} placeholder={label} />
//             {touched && error && <span>{error}</span>}
//         </div>
//     </div>
// );

const renderField = ({
                             input,
                             label,
                             type,
                             meta: { touched, error, invalid },
                             ...custom
                         }) => (
    <TextField
        label={label}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
    />
);

const renderPhones = ({ fields, meta: { error } }) => (
    <div>
        <Button variant="contained" onClick={() => fields.push()}>
            Add Phone
        </Button>
        <List>
        {fields.map((phone, index) => (
            <ListItem key={index}>
                <Field
                    name={phone}
                    type="text"
                    component={renderField}
                    label={`Phone #${index + 1}`}
                />
                <Button variant="contained" color="secondary" onClick={() => fields.remove(index)}>
                    Remove Phone
                </Button>
            </ListItem>
        ))}
            {error && <ListItem className="error">{error}</ListItem>}
        </List>

    </div>
)

const renderTags = ({ fields, meta: { error } }) => (
    <div>
        <Button variant="contained" onClick={() => fields.push()}>
            Add Tag
        </Button>
        <List>
            {fields.map((tag, index) => (
                <ListItem key={index}>
                    <Field
                        name={tag}
                        type="text"
                        component={renderField}
                        label={`Tag #${index + 1}`}
                    />
                    <Button variant="contained" color="secondary" onClick={() => fields.remove(index)}>
                        Remove Tag
                    </Button>
                </ListItem>
            ))}
            {error && <ListItem className="error">{error}</ListItem>}
        </List>

    </div>
)

const renderEmails = ({ fields, meta: { error } }) => (
    <div>
        <Button variant="contained" onClick={() => fields.push()}>
            Add Email
        </Button>
        <List>
        {fields.map((email, index) => (
            <ListItem key={index}>
                <Field
                    name={email}
                    type="text"
                    component={renderField}
                    label={`Email #${index + 1}`}
                />
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => fields.remove(index)}
                >
                    Remove Email
                </Button>
            </ListItem>
        ))}
            {error && <ListItem className="error">{error}</ListItem>}
        </List>

    </div>
)

const renderSocials = ({ fields, meta: { error } }) => (
    <div>
        <Button variant="contained" onClick={() => fields.push()}>
            Add Social
        </Button>
        <List>
            {fields.map((social, index) => (
                <ListItem key={index}>
                    <List>
                        <ListItem>
                            <Field
                                name={`${social}.type`}
                                type="text"
                                component={renderField}
                                label="Type"
                            />
                        </ListItem>
                        <ListItem>
                            <Field
                                name={`${social}.profile_link`}
                                type="text"
                                component={renderField}
                                label="Profile Link"
                            />
                        </ListItem>
                        <ListItem>
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={() => fields.remove(index)}
                            >
                                Remove Email
                            </Button>
                        </ListItem>
                    </List>
                </ListItem>
            ))}
            {error && <ListItem className="error">{error}</ListItem>}
        </List>

    </div>
)

const useStyles = makeStyles({
    card: {
        minWidth: 200,
        padding: 10,
        marginBottom: 10,
        marginUp: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

const renderContacts = ({ fields, meta: { error, submitFailed } }) => (

    <div>
        <Button variant="outlined" onClick={() => fields.push({})}>
            Add Contact
        </Button>
        {submitFailed && error && <span>{error}</span>}

        {fields.map((contact, index) => (
            <Card className={useStyles.card} key={index}>

                <CardContent>
                    <h4>Contact #{index + 1}</h4>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => fields.remove(index)}
                    >
                        Remove Contact
                    </Button>
                    <List>
                        <ListItem key={index}>

                            <Field
                                name={`${contact}.firstname`}
                                type="text"
                                component={renderField}
                                label="First Name"
                            />
                            <Field
                                name={`${contact}.lastname`}
                                type="text"
                                component={renderField}
                                label="Last Name"
                            />
                        </ListItem>
                        <ListItem>
                            <FieldArray name={`${contact}.emails`} component={renderEmails} />
                        </ListItem>
                        <ListItem>
                            <FieldArray name={`${contact}.phones`} component={renderPhones} />
                        </ListItem>
                        <ListItem>
                            <FieldArray name={`${contact}.tags`} component={renderTags} />
                        </ListItem>
                        <ListItem>
                            <FieldArray name={`${contact}.socials`} component={renderSocials} />
                        </ListItem>
                    </List>
                </CardContent>
            </Card>
        ))}

    </div>
)

const FieldArraysForm = props => {
    const { handleSubmit, pristine, reset, submitting } = props;

    return (
        <Form  onSubmit={handleSubmit} >
            <FieldArray name="Contacts" component={renderContacts} />
            <div>
                <Button type="submit" variant="contained" color="primary" disabled={submitting}>
                    Submit
                </Button>
                <Button variant="contained" color="secondary" disabled={pristine || submitting} onClick={reset}>
                    Clear Values
                </Button>
            </div>
        </Form >
    );
};


export default reduxForm({
    form: 'fieldArrays', // a unique identifier for this form
    validate
})(FieldArraysForm)
