import React from 'react';
import FieldArraysForm from "./Form";
import connect from "react-redux/es/connect/connect";
import * as contactActions from "../_actions/";
import Dialog from "../_components/AlertDialog"

class FormInsertContact extends React.Component {


    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.submitForce = this.submitForce.bind(this);
    }

    componentDidMount() {

    }

    submit(values) {
        this.props.saveContacts(values);
    };

    submitForce(values) {
        this.props.saveContacts(values);

    };

    render() {

        const { contacts } = this.props;

        const handleClose = () => {
            this.open = false;
        };
        return(
        <div style={{ padding: 15 }}>
            {contacts.force_contacts && contacts.force_contacts.length > 0 && <Dialog contacts={contacts.force_contacts} onAgree={this.submitForce}/> }
            <h2>New Contacts</h2>
            <FieldArraysForm onSubmit={this.submit}  />
        </div>
    )}
}

const actionCreators = {
    getContacts:    contactActions.getContacts,
    deleteContacts: contactActions.deleteContacts,
    saveContacts:   contactActions.saveContacts,
    changePage:      contactActions.changePage,
    changeRowForPage: contactActions.changeRowForPage,
};

function mapStateToProps(state) {
    const { contacts, authentication , open } = state;

    return { contacts, authentication, open};
}

const connectedFormInsertContact = connect(mapStateToProps, actionCreators)(FormInsertContact);
export { connectedFormInsertContact as FormInsertContact };

