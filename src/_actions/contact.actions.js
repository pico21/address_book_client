import { contactConstants } from '../_constants';
import { contactService } from '../_services';
// import {store} from '../_helpers/store'

export function changeRowForPage(args, rowforpage){
    return dispatch => {
        dispatch(rowforpageChange(rowforpage));
    };

    function rowforpageChange(number) { return {type: contactConstants.ROW_FOR_PAGE_CHANGE, number}}
}

export function changePage(args, page) {

    return dispatch => {
        dispatch(pageChange(page));

    };

    function pageChange(page) { return {type: contactConstants.PAGE_CHANGE, page}}
}

export function getContacts(args) {

    return dispatch => {

        dispatch(request());

        contactService.getContacts(args)
            .then(
                data => dispatch(success(data)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: contactConstants.GETALL_REQUEST } }
    function success(contacts) { return { type: contactConstants.GETALL_SUCCESS, contacts } }
    function failure(error) { return { type: contactConstants.GETALL_FAILURE, error } }
}

export function saveContacts(contacts, force = false) {

    return dispatch => {
        // const state = store.getState();
        // console.log("state",state.contacts);
        dispatch(request(contacts));

        return contactService.saveContacts(contacts, force)
            .then(
                contacts => {
                    dispatch(success(contacts));
                    if (contacts.force){
                        dispatch(request_force(contacts.force));
                    }
                },
                error => dispatch(failure(error.toString()))
            );
    };

    function request_force(contacts) { return { type: contactConstants.SAVE_REQUEST_FORCE, contacts } }
    function request(contacts) { return { type: contactConstants.SAVE_REQUEST, contacts } }
    function success(contacts) { return { type: contactConstants.SAVE_SUCCESS, contacts } }
    function failure(error) { return { type: contactConstants.SAVE_FAILURE, error } }

}

export function deleteContacts(contacts){
    return dispatch => {
        dispatch(request(contacts));

        contactService.deleteContacts(contacts)
            .then(
                contacts => dispatch(success(contacts)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request(contacts) { return { type: contactConstants.DELETE_REQUEST, contacts } }
    function success(contacts) { return { type: contactConstants.DELETE_SUCCESS, contacts } }
    function failure(error) { return { type: contactConstants.DELETE_FAILURE, error } }
}

