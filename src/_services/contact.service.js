import config from 'config';
import {userService} from "../_services/user.service"
import { authHeader } from '../_helpers';
import * as contactActions from "../_actions";

export const contactService = {
    getContacts,
    saveContacts,
    deleteContacts,
};

function getContacts(args){
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
    };
    const queryString = objToQueryString(args);
    return fetch(`${config.apiUrl}/api/contacts?${queryString}`, requestOptions)
        .then(handleResponse)
        // .then(tableContactsHandler)
}

function saveContacts(contacts, force = false){
    let body = contacts.contacts ? {contacts: contacts, force: force} : contacts;
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
    };
    return fetch(`${config.apiUrl}/api/contacts`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        })
}

function deleteContacts(contacts){
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({contacts: contacts})
    }
    return fetch(`${config.apiUrl}/api/contacts`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        })
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (response.status >= 400 && response.status < 500 ) {

            userService.logout();
            location.reload(true);
        }
        if (!response.ok) {


            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

function objToQueryString(obj) {
    const keyValuePairs = [];
    for (const key in obj) {
        keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return keyValuePairs.join('&');
}