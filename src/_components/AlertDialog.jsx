import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {List, ListItem} from '@material-ui/core'

export default function AlertDialog({ contacts, onAgree }) {
    const [open, setOpen] = React.useState(true);

    const handleClose = () => {
        setOpen(false);
    };

    const handleAgree = () => {
        setOpen(false);
        onAgree(contacts);
    };

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Conferma l'inserimento dei Contatti"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Please confirm this contacts
                        <List>
                            {contacts && contacts.map((contact, index) => (
                                <ListItem key={index}>
                                    <span>{index}</span>
                                    <span>{contact.firstname}</span>
                                    <span>{contact.lastname}</span>
                                    {contact.emails && <span>{contact.emails.map((elem, index) => {return elem.email;}).join(", ")}</span>}
                                    {contact.phones && <span>{contact.phones.map((elem, index) => {return elem.number;}).join(", ")}</span>}
                                    {contact.tags && <span>{contact.tags.map((elem, index) => {return elem.name;}).join(", ")}</span>}
                                    {contact.socials && <span>{contact.socials.map((elem, index) => {return elem.type;}).join(", ")}</span>}
                                </ListItem>
                                ))
                            }
                        </List>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Disagree
                    </Button>
                    <Button onClick={handleAgree} color="primary" autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}