import { contactConstants } from '../_constants';

const initState = {
    loading: true,
    items: [["Caricamento..."]],
    error: null,
    contacts_arguments: {
        page: 0,
        total: 0,
        firstname: "",
        lastname: "",
        tag: "",
        email: "",
        phone: "",
        sorts: "",
        search: "",
        limit: 10,
    }
};

export function contacts(state = initState, action) {
  switch (action.type) {
      case contactConstants.ROW_FOR_PAGE_CHANGE: {
          let Contacts_arguments = state.contacts_arguments;
          Contacts_arguments.limit = action.number;
          return {
              ...state,
              contacts_arguments: Contacts_arguments
          }
      }
      case contactConstants.PAGE_CHANGE: {
          let Contacts_arguments = state.contacts_arguments;
          Contacts_arguments.page = action.page;
          return {
              ...state,
              contacts_arguments: Contacts_arguments
          }
      }
      case contactConstants.GETALL_REQUEST:
      return {
          ...state,
            loading: true,

      };
      case contactConstants.GETALL_SUCCESS:
          let Contacts_arguments = state.contacts_arguments;
          Contacts_arguments.total = parseInt(action.contacts.total);
      return {
          ...state,
            items: action.contacts.Contacts,
          contacts_arguments: Contacts_arguments
      };
      case contactConstants.GETALL_FAILURE:
      return {
          ...state,
        // error: action.error
      };

    case contactConstants.SAVE_REQUEST:
        return {
            ...state,
            loading: true,
            force_contacts: null,
        };
    case contactConstants.SAVE_REQUEST_FORCE:
      return {
          ...state,
          loading: false,
          force_contacts: action.contacts
      };
    case contactConstants.SAVE_SUCCESS:
        return {
            ...state,
            items: action.contacts
        };
    case contactConstants.SAVE_FAILURE:
        return {
            ...state,
            error: action.error
        };

    case contactConstants.DELETE_REQUEST:

      return {
        ...state,
        items: state.items.map(contacts =>
          contact.id === action.id
            ? { ...contact, deleting: true }
            : contact
        )
      };
      case contactConstants.DELETE_SUCCESS:

      return {
          ...state,
        items: state.items.filter(contact => contact.id !== action.id)
      };
    case contactConstants.DELETE_FAILURE:

      return {
        ...state,
        items: state.items.map(contact => {
          if (contact.id === action.id) {
            const { deleting, ...contactCopy } = contact;
            // return copy of user with 'deleteError:[error]' property
            return { ...contactCopy, deleteError: action.error };
          }

          return contact;
        })
      };
    default:
      return state
  }
}