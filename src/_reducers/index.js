import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import {contacts} from "./contacts.reducer";
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { reducer as reduxFormReducer } from 'redux-form';

const rootReducer = combineReducers({
  authentication,
    form: reduxFormReducer,
  registration,
  users,
    contacts,
  alert
});

export default rootReducer;