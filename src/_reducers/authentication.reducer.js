import { userConstants } from '../_constants';

let localWsse = JSON.parse(localStorage.getItem('wsse'));
const initialState = localWsse ? { loggedIn: true, wsse:localWsse } : {};

export function authentication(state = initialState, action) {

  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {...state,
        loggingIn: true,
        wsse: null,
      };
    case userConstants.LOGIN_SUCCESS:
      return {...state,
        loggedIn: true,
        wsse: action.user.credentials
      };
      case userConstants.LOGIN_FAILURE:

      return {...state, loggedIn: false,wsse: null};
    case userConstants.LOGOUT:

      return {...state,loggedIn: false,loggingIn:false,
          wsse: null};
    default:
      return state
  }
}