import React, {Fragment} from "react";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import connect from "react-redux/es/connect/connect";
import { history } from '../_helpers';

const classes = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {


        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>

                        </Typography>
                        {!this.props.authentication.loggedIn ?
                            <Button color="inherit" onClick={() => changePage("/login")}>Login</Button>
                            : <Button color="inherit" onClick={() => changePage("/logout")}>Logout</Button>}
                        <Button color="inherit" onClick={() => changePage("/contacts")}>Contacts</Button>
                        <Button color="inherit" onClick={() => changePage("/new-contact")}>New Contacts</Button>

                    </Toolbar>
                </AppBar>
            </div>
        );
    }

}

function changePage(path){
    history.push(path);
}

function mapStateToProps(state) {

    const { contacts, authentication, page, count } = state;
    return { contacts, authentication, page, count };
}

const connectedHeader = connect(mapStateToProps)(Header);
export { connectedHeader as Header };